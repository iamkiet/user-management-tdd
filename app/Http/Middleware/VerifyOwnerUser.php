<?php

namespace App\Http\Middleware;

use Closure;

class VerifyOwnerUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = $request->id;

        $userLoggedId = auth()->user()->id;

        if ($userId != $userLoggedId) {
            return response(null, 401);
        }

        return $next($request);
    }
}
