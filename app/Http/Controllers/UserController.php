<?php

namespace App\Http\Controllers;

use App\User;
use App\Services\UserServiceContract;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $service;

    public function __construct(UserServiceContract $service) {
        $this->service = $service;
        $this->middleware('auth', ['only' => ['update', 'destroy']]);
        $this->middleware('owner-user', ['only' => ['update', 'destroy']]);
    }

    public function index()
    {
        $users = $this->service->paginate();

        return view('welcome', ['users' => $users]);
    }

    public function create()
    {
        //
    }

    public function store(UserCreateRequest $request)
    {
        $user = $this->service->store($request->all());
        return response()->json(['user' => $user], 201);
    }

    public function show($id)
    {

        $user = $this->service->find($id);
        return response()->json($user);
    }

    public function edit($id) {
        $user = $this->service->find($id);
        return view('user.edit', $user);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $this->service->update($id, $request->all());

        return redirect('/')->with('status', 'user updated');
    }

    public function destroy($id)
    {
        $this->service->destroy($id);

        return redirect('/')->with('status', 'user deleted');
    }
}
