# **USER MANAGEMENT PACKGIST WITH TDD**

# TEST CIRCUMSTANCE

# MODEL

> Model testcase make sure that user model can work with basic handle such as read, create, update and delete

## User table has email and password
    give data
    when create new user by factory engine
    then expect that new user in database

## User table has password was hashed
    give data
    when create new user by factory engine
    then expect that user 'passowrd in database was hashed

## User model can update
    give modify data and create new user by factory engine
    when we change some field of user with modify data
    then expect that new value of user in database

## User model can delete
    give data and create new user by factory engine
    when delete user recent create
    then expect that user not existed in database

# CONTROLLER

> ROUTES LIST AND PERMISSION
* GET - index, show, edit, delete
* POST - store
* PUT/PATCH - update
* DELETE - destroy
* Permission: user, admin

> PROCESS TESTCASE
## Index routing can works
    direct to index route
    then expect that status code is 200

## Index routing should return user list
    give ten user
    when direct to index route
    then expect that status code is 200 and recive users json array

## Show routing should return user detail
    give one user
    when direct to show route with user id recent create
    then expect that status code is 200 and recive user json

## Edit routing should be redirect update route
    redirect to edit routing with user id
    then expect that user info include in edit site

## Store routing can create new user
    give request data
    when direct to store route with request data above
    then expect that status code is 201 and recive user json

## Update routing can update user
    give one user and request modify data
    when direct to update route with user id and request modify data above
    then expect that status code is 204 and recive user json match modify data

## Destroy routing can delete user
    give one user
    when direct to destroy route with user id above
    then expect that status code is 204

> MIDDLEWARE TESTCASE

## Store routing has validate fields
    give request data invalid
    when direct to store route with request data
    then expect that status 422

## Update routing has validate fields
    give request data invalid
    when direct to update route with request data
    then expect that status 422

## Unauthenticated cant access update routing
    give one user
    when direct to update routing
    then expect status 401 and user unthorization

## Unauthenticated cant access destroy routing
    give one user
    when direct to destroy routing
    then expect status 401 and user unthorization

## Unauthorized cant modify user
    give two user
    and request data
    and acting with first user
    when direct to update route with second user id and request data
    then expect that status code is 401 - unauthorized

## Unauthorized cant delete user
    give two user
    and acting with first user
    when direct to destroy route with second user id
    then expect that status code is 401 - unauthorized

> Controller testcase above make sure that user controller work logically

# BROWSER
## INDEX SITE
### Its contains list of user
    give two user
    when redirect index routing
    then expect that user lists exist insite

### Its has add add user button and its work when click
    redirect index routing
    then expect that add user button exist insite
    when click to add user button
    then expect that current path is create routing

## LOGIN SITE
### its contain login form
    give one user
    when direct login routing
    and fill in input fields
    and click to login button
    then expect that path is home routing
    and logout to destroy session

### its appear errors message when invalid input
    give one user
    when direct login routing
    and fill in input fields with invalid value
    and click to login button
    then expect that path is login routing
    and match errors

## REGISTER SITE
### its contain login form
    give request data
    when direct register routing
    and fill in input fields
    and click to register button
    then expect that path is home routing
    and logout to destroy session

### its appear errors message when invalid input
    give request data
    when direct register routing
    and fill in input fields with invalid value
    and click to register button
    then expect that path is register routing
    and match errors
