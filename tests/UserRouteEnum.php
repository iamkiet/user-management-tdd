<?php

namespace Tests;

abstract class UserRouteEnum
{
    const INDEX = ['method' => 'get', 'route' => '/'];
    const STORE = ['method' => 'post', 'route' => '/users'];
    const SHOW = ['method' => 'get', 'route' => '/users/'];
    const EDIT = ['method' => 'get', 'route' => '/users/edit/'];
    const UPDATE = ['method' => 'put', 'route' => '/users/'];
    const DESTROY = ['method' => 'delete', 'route' => '/users/'];
}
