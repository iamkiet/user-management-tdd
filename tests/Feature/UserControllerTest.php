<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Tests\UserRouteEnum;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    protected $user = null;

    //process
    protected function getUserId() {
        return $this->user->id;
    }

    protected function getUserEmail() {
        return $this->user->email;
    }

    protected function getUserName() {
        return $this->user->name;
    }

    public function setUp() {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function test_index_route_can_work() {
        $response = $this->call(UserRouteEnum::INDEX['method'], UserRouteEnum::INDEX['route']);

        $response->assertStatus(200);
    }

    public function test_index_route_should__return_lists_user() {
        $users = factory(User::class, 10)->create();

        $response = $this->call(UserRouteEnum::INDEX['method'], UserRouteEnum::INDEX['route']);
        $response->assertOk()
                ->assertViewHasAll([
                 'users'
                ]);
    }

    public function test_show_route_should_return_user_detail() {
        $userId = $this->user->id;

        $response = $this->call(UserRouteEnum::SHOW['method'], UserRouteEnum::SHOW['route'].$userId);

        $response->assertOk()
                ->assertJsonStructure([
                    'id',
                    'email',
                    'name'
                ]);
    }

    public function test_edit_routing_should_be_return_user_detail() {

        $response = $this->call(UserRouteEnum::EDIT['method'], UserRouteEnum::EDIT['route'].$this->getUserId());

        $response->assertOk()
                ->assertViewHasAll([
                    'email' => $this->getUserEmail(),
                    'name' => $this->getUserName()
                ]);
    }

    public function test_store_route_should_create_user() {
        $this->withoutMiddleware();

        $request = [
            'email' => 'kiet1@gmail.com',
            'password' => '123456',
            'name' => 'kiet nguyen'
        ];

        $response = $this->json(UserRouteEnum::STORE['method'], UserRouteEnum::STORE['route'], $request);
        $response->assertStatus(201)
                ->assertJsonFragment([
                    'email' => $request['email'],
                    'name' => $request['name']
                ]);
    }

    public function test_update_route_should_update_user() {
        $this->withoutMiddleware();

        $request = [
            'name' => 'kiet nguyen'
        ];

        $response = $this->json(UserRouteEnum::UPDATE['method'], UserRouteEnum::UPDATE['route'].$this->getUserId(), $request);
        $response->assertLocation('/')
                ->assertSessionHas('status', 'user updated');
    }

    public function test_destroy_route_should_delete_user() {
        $this->withoutMiddleware();

        $response = $this->call(UserRouteEnum::DESTROY['method'], UserRouteEnum::DESTROY['route'].$this->getUserId());
        $response->assertLocation('/')
                ->assertSessionHas('status', 'user deleted');
    }

    // MARK -------------MIDDLEWARE TESTING
    public function test_store_route_has_validate_fields() {
        $this->withoutMiddleware();

        $r = new UserCreateRequest();

        $this->assertEquals([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ], $r->rules());

        $request = [
            'name' => '',
        ];

        $response = $this->json(UserRouteEnum::STORE['method'], UserRouteEnum::STORE['route'], $request);
        $response->assertStatus(422);
    }

    public function test_update_route_has_validate_fields() {
        $this->withoutMiddleware();

        $r = new UserUpdateRequest();

        $this->assertEquals([
            'name' => 'required',
        ], $r->rules());

        $request = [
            'name' => '',
        ];

        $response = $this->json(UserRouteEnum::UPDATE['method'], UserRouteEnum::UPDATE['route'].$this->getUserId(), $request);
        $response->assertStatus(422);
    }

    public function test_unauthorization_cant_access_update_routing() {
        $request = [
            'name' => 'kiet nguyen',
        ];

        $this->assertGuest();

        $response = $this->json(UserRouteEnum::UPDATE['method'], UserRouteEnum::UPDATE['route'].$this->getUserId(), $request);
        $response->assertStatus(401);
    }

    public function test_unauthenticated_cant_access_destroy_routing() {
        $this->assertGuest();

        $response = $this->json(UserRouteEnum::DESTROY['method'], UserRouteEnum::DESTROY['route'].$this->getUserId());
        $response->assertStatus(401);
    }

    public function test_unauthenticated_cant_modify_owner_user() {

        $ortherUser = factory(User::class)->create();

        $request = [
            'name' => 'kiet nguyen',
        ];

        $this->actingAs($ortherUser);
        $this->assertAuthenticated();

        $response = $this->json(UserRouteEnum::UPDATE['method'], UserRouteEnum::UPDATE['route'].$this->getUserId(), $request);
        $response->assertStatus(401);
    }

    public function test_unauthenticated_cant_destroy_owner_user() {

        $ortherUser = factory(User::class)->create();

        $this->actingAs($ortherUser);
        $this->assertAuthenticated();

        $response = $this->call(UserRouteEnum::DESTROY['method'], UserRouteEnum::DESTROY['route'].$this->getUserId());
        $response->assertStatus(401);
    }
}
