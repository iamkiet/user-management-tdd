<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
class IndexTest extends DuskTestCase
{

    public function test_its_contains_user_list() {
        $users = factory(User::class, 2)->create();

        $this->browse(function (Browser $browser) use ($users) {
            $browser->visit('/')
                    ->assertSee($users[0]->name)
                    ->assertSee($users[1]->name)
                    ->click('@add-user-button')
                    ->assertPathIs('/users/create');
        });
    }

    public function test_its_has_add_user_button()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('ADD USER')
                    ->click('@add-user-button')
                    ->assertPathIs('/users/create');
        });
    }
}
