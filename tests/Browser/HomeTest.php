<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends DuskTestCase
{
    public function test_its_direct_to_login_if_unauthorized() {
        $this->browse(function ($browser) {
            $browser->visit('/home')
                    ->assertPathIs('/login');
        });
    }

    public function test_its_contains_user_name() {

        $user = factory(User::class)->create();

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/home')
                    ->assertPathIs('/login')
                    ->type('email', $user->email)
                    ->type('password', 'secret')
                    ->press('Login')
                    ->assertPathIs('/home')
                    ->assertSee($user->name)
                    ->logout();
        });
    }
}
