<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterTest extends DuskTestCase
{
    use RefreshDatabase;

    public function test_register_form_has_name_email_password_fields()
    {

        $this->browse(function ($browser) {
            $browser->visit('/register')
                    ->type('name', 'kiet nguyen')
                    ->type('email', 'kiet2@gmail.com')
                    ->type('password', '123456')
                    ->type('password_confirmation', '123456')
                    ->press('Register')
                    ->assertPathIs('/home')
                    ->logout();
        });
    }

    public function test_its_appear_errors_message_when_invalid_input() {
        $this->browse(function ($browser) {
            $browser->visit('/register')
                    ->type('name', 'kiet nguyen')
                    ->type('email', 'kiet1@gmail.com')
                    ->type('password', '123456')
                    ->type('password_confirmation', 'sdf')
                    ->press('Register')
                    ->assertPathIs('/register')
                    ->assertSee('The password confirmation does not match.');
        });
    }
}
