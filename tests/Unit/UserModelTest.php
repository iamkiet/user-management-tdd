<?php

namespace Tests\Unit;

use Hash;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserModelTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp() {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function test_user_table_has_email_and_password() {
        $givenData = $this->givenData();

        $user = User::create($givenData);

        $this->assertDatabaseHas('users', [
            'email' => $user->email,
            'password' => $user->password
        ]);
    }

    public function test_user_table_has_password_was_hashed() {
        $givenData = $this->givenData();

        $user = User::create($givenData);

        $isHash = Hash::check('123456', $user->password);
        $this->assertEquals($isHash, 1);
    }

    public function test_user_model_can_update() {
        $givenData = [
            'name' => 'kiet nguyen'
        ];

        $this->user->update($givenData);

        $this->assertDatabaseHas('users', [
            'name' => $this->user['name']
        ]);
    }

    public function test_user_model_can_delete() {
        $this->user->delete();

        $this->assertDatabaseMissing('users', [
            'name' => $this->user['name']
        ]);
    }

    protected function givenData() {
        $givenData = [
            'email' => 'kiet1@gmail.com',
            'password' => Hash::make('123456'),
            'name' => 'kiet nguyen'
        ];
        return $givenData;
    }
}
