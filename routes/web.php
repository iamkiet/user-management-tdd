<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/create', 'UserController@create')->name('user.create');

Route::get('/users/edit/{id}', 'UserController@edit')->name('user.edit');
Route::get('/', 'UserController@index');
Route::get('/users/{id}', 'UserController@show');

Route::post('/users', 'UserController@store');
Route::put('/users/{id}', 'UserController@update')->name('user.update');
Route::delete('/users/{id}', 'UserController@destroy');
